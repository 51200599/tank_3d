using UnityEngine;
using System.Collections;


public class destroyOnDamage : MonoBehaviour {
	
	public GameObject ExplosionFX;
	
	public GUIText mText;
	public bool isGameOver = false;
	public static int numPlayerDie =0;
	public Item item;
	public int weapon;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void TakeDamage(GameObject shooter)
	{	
		if ((shooter.GetComponent<AiTank>() != null) && (GetComponent<AiTank>() != null))
			return;
		
		if (ExplosionFX != null)
			Instantiate(ExplosionFX, transform.position, Quaternion.identity);

		if (gameObject.tag == "Player") {
			numPlayerDie++;
			if (NetworkManager.choice == 1 && numPlayerDie == 1) {
				isGameOver = true;
				Debug.Log("1 lose");
				Debug.Log(isGameOver);
			}
			if(NetworkManager.choice == 2 && numPlayerDie==2){
				isGameOver = true;
				Debug.Log("2 lose");
			//Debug.Log(gameObject.tag);
			}
		}
		if (isGameOver)
		{
			Debug.Log("vao game over lose");
			Application.LoadLevel ("YouLose");
			//mText.gameObject.active = true;
			GUI.Label(new Rect(300,300,80,100), "You Lose!!!!");


			if (GUI.Button (new Rect (300, 400, 100, 70), "Main Menu"))
				Application.LoadLevel ("Menu");

			//Debug.Log("dasdiajsk");
			//Debug.Log(mText.gameObject.active);
			
		}
		if (gameObject.tag == "EnemyTag") {
			randomItem();
		}

		if (gameObject.tag == "EnemyWeaponTag") {
			weapon--;
		}

		if(weapon==0)
			DestroyObject (this.gameObject);
	}

	private void randomItem(){
		int random = Random.Range (0, 100);
		if (random <= 50) {
			Instantiate(item,this.gameObject.transform.position,Quaternion.identity);
		}
	} 
}
