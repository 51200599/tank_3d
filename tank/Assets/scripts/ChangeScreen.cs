﻿using UnityEngine;

using System.Collections;

public class ChangeScreen : MonoBehaviour {

	static public int choice;

	public void ChangeToScene (int _choice) {
		choice = _choice;
		Application.LoadLevel ("tanks");
	}

	public void ChangeToScene (string _choice) {
		Application.LoadLevel (_choice);
	}

	public void Quit() {
		Application.Quit ();
	}
}
